﻿namespace Tech_No_Logic_WPF
{
    public enum ApplicationPage
    {
        /// <summary>
        /// The initial login page
        /// </summary>
        Login = 0,
        Register = 1,
        Layout = 2
    }
}
