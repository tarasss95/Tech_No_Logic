﻿using Barter.DAL;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tech_No_Logic.Models;

namespace Tech_No_Logic.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private UnitOfWork unitOfWork = null;

        public AdminController()
        {
            unitOfWork = new UnitOfWork();
        }

        public AdminController(UnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        // GET: Admin
        [HttpGet]
        public ActionResult Index()
        {
            BarterContext context = new BarterContext();
            var users = unitOfWork.Repository<ApplicationUser>().GetAll() as IEnumerable<ApplicationUser>;
            var model = new Collection<UserModeratorViewModel>();

            var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var moderator = RoleManager.FindByName("Moderator");

            foreach (var user in users)
            {
                model.Add(new UserModeratorViewModel()
                {
                    Id = user.Id,
                    LName = user.LName,
                    FName = user.FName
                });
                if (user.Roles.Any(role => role.RoleId == moderator.Id))
                {
                    model.Last().isModerator = true;
                }
            }

            return View("Index", model);
        }

        //[HttpGet]
        public ActionResult AddModerator(string UserId)
        {
            BarterContext context = new BarterContext();
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            UserManager.AddToRole(UserId, "Moderator");
            return RedirectToAction("Index");
        }

        public ActionResult RemoveModerator(string UserId)
        {
            BarterContext context = new BarterContext();
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            UserManager.RemoveFromRole(UserId, "Moderator");
            return RedirectToAction("Index");
        }

        public ActionResult RemoveUser(string UserId)
        {
            BarterContext context = new BarterContext();
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            // removing from role User
            UserManager.RemoveFromRole(UserId, "User");

            // removing as application user
            ApplicationUser user = unitOfWork.Repository<ApplicationUser>().Get(c => c.Id == UserId);
            unitOfWork.Repository<ApplicationUser>().Delete(user);
            unitOfWork.SaveChanges();

            return RedirectToAction("Index");

        }
      
    }
}