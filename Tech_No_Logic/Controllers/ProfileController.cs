﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Tech_No_Logic.Models;
using Barter.DAL;

namespace Tech_No_Logic.Controllers
{
    [Authorize]
    [RequireHttps]
    public class ProfileController : Controller
    {
        private ApplicationUserManager _userManager;
        private UnitOfWork unitOfWork;
        public ProfileController()
        {
            unitOfWork = new UnitOfWork();
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        public ActionResult Index()
        {
            var userId = User.Identity.GetUserId();
            var userController = new MeController(UserManager);
            var userViewModel = userController.Get();
            var model = new ProfileViewModel
            {
                FirstName = userViewModel.FirstName,
                LastName = userViewModel.LastName,
                HomeTown = userViewModel.Hometown,
                UserEmail = userViewModel.Email,
                Age = userViewModel.Age,
                Photo = userViewModel.Photo,
                PhoneNumber = UserManager.GetPhoneNumber(userId),
            };
            return View(model);
        }

        public ActionResult RequestIndex(string userId)
        {
            var user = UserManager.FindById(userId);
            var model = new ProfileViewModel
            {
                Id=user.Id,
                FirstName = user.FName,
                LastName = user.LName,
                HomeTown = user.Hometown,
                UserEmail = user.Email,
                Age = user.Age,
                Photo = user.Photo,
                PhoneNumber = UserManager.GetPhoneNumber(userId),
            };
            return View(model);
        }
        public ActionResult OtherUserSkills(string userId)
        {
            var skills = unitOfWork.Repository<UserSkill>().GetAll().Where(skill => skill.ApplicationUser.Id == userId) as IEnumerable<UserSkill>;
            return PartialView("~/Views/UserSkills/RequestIndex.cshtml", skills);
        }
        public ActionResult OtherUserSkillWanteds(string userId)
        {
            var skills = unitOfWork.Repository<UserSkillWanted>().GetAll().Where(skill => skill.ApplicationUser.Id == userId) as IEnumerable<UserSkillWanted>;
            return PartialView("~/Views/UserSkillWanteds/RequestIndex.cshtml", skills);
        }
        public ActionResult UserSkills()
        {
            string userId = User.Identity.GetUserId();
            var skills = unitOfWork.Repository<UserSkill>().GetAll().Where(skill => skill.ApplicationUser.Id == userId) as IEnumerable<UserSkill>;
            return PartialView("~/Views/UserSkills/Index.cshtml", skills);
        }
        public ActionResult UserSkillWanteds()
        {
            string userId = User.Identity.GetUserId();
            var skills = unitOfWork.Repository<UserSkillWanted>().GetAll().Where(skill => skill.ApplicationUser.Id == userId) as IEnumerable<UserSkillWanted>;
            return PartialView("~/Views/UserSkillWanteds/Index.cshtml", skills);
        }
    }
}