﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Barter.DAL;
using Tech_No_Logic.Models;
using Microsoft.AspNet.Identity;

namespace Tech_No_Logic.Controllers
{
    [Authorize]
    public class UserSkillsController : Controller
    {
        private UnitOfWork unitOfWork = null;

        public UserSkillsController()
        {
            unitOfWork = new UnitOfWork();
        }

        public UserSkillsController(UnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        public ActionResult Search(string searchString)
        {
            if (String.IsNullOrEmpty(searchString)) return View();
            var skills = unitOfWork.Repository<UserSkill>().GetAll()
                .Where(s => s.Name.ToLower().Contains(searchString.ToLower()));
            return View(skills.ToList());
        }
        public ActionResult Index()
        {
            string userId = User.Identity.GetUserId();
            var skills = unitOfWork.Repository<UserSkill>().GetAll().Where(skill => skill.ApplicationUser.Id == userId) as IEnumerable<UserSkill>;

            return PartialView("~/Views/UserSkills/Index.cshtml", skills);
        }

        // GET: UserSkills/Create
        public ActionResult Create()
        {
            ViewBag.UserId = Guid.Parse(User.Identity.GetUserId());
            ViewBag.SubrubricId = new SelectList(unitOfWork.Repository<Subrubric>().GetAll() as IEnumerable<Subrubric>, "Id", "Name");
            return View();
        }

        // POST: UserSkills/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UserSkillViewModel userSkill)
        {
            if (ModelState.IsValid)
            {
                string userId = User.Identity.GetUserId();
                var user = unitOfWork.Repository<ApplicationUser>().Get(u => u.Id == userId);
                var skill = new UserSkill()
                {
                    Name = userSkill.Name,
                    Description = userSkill.Description,
                    UserId = userId,
                    SubrubricId = userSkill.SubrubricId
                };
                unitOfWork.Repository<UserSkill>().Add(skill);
                unitOfWork.SaveChanges();

                var skills = unitOfWork.Repository<UserSkill>().GetAll().Where(s => s.UserId == userId);
                return RedirectToAction("Index", "Profile");
            }
            return RedirectToAction("Index", "Profile");
        }

        // GET: UserSkills/Edit/5
        public ActionResult Edit(int id = 0)
        {
            UserSkill skill = unitOfWork.Repository<UserSkill>().Get(c => c.Id == id);
            if (skill == null)
            {
                return HttpNotFound();
            }
            ViewBag.SubrubricId = new SelectList(unitOfWork.Repository<Subrubric>().GetAll() as IEnumerable<Subrubric>, "Id", "Name", skill.SubrubricId);
            return View(skill);

        }

        // POST: UserSkills/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(UserSkill userSkill)
        {
            if (userSkill.Name != string.Empty)
            {
                unitOfWork.Repository<UserSkill>().Attach(userSkill);
                unitOfWork.SaveChanges();
                return RedirectToAction("Index", "Profile");
            }
            return RedirectToAction("Index", "Profile");

        }

        

        // GET: UserSkills/Delete/5
        public ActionResult Delete(int id = 0)
        {
            UserSkill skill = unitOfWork.Repository<UserSkill>().Get(c => c.Id == id);
            if (skill == null)
            {
                return HttpNotFound();
            }
            return View(skill);
        }

        // POST: UserSkills/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UserSkill skill = unitOfWork.Repository<UserSkill>().Get(c => c.Id == id);
            unitOfWork.Repository<UserSkill>().Delete(skill);
            unitOfWork.SaveChanges();
            return RedirectToAction("Index", "Profile");
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
