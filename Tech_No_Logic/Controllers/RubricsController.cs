﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Barter.DAL;
using Tech_No_Logic.Models;

namespace Tech_No_Logic.Controllers
{
    [Authorize(Roles = "Moderator")]
    public class RubricsController : Controller
    {
        private UnitOfWork unitOfWork = null;

        public RubricsController()
        {
            unitOfWork = new UnitOfWork();
        }

        public RubricsController(UnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        // GET: Rubrics
        public ActionResult Index()
        {
            return View(unitOfWork.Repository<Rubric>().GetAll() as IEnumerable<Rubric>);
        }

        // GET: Rubrics/Details/5
        public ActionResult Details(int id = 0)
        {
            Rubric rubric = unitOfWork.Repository<Rubric>().Get(c => c.Id == id);
            if (rubric == null)
            {
                return HttpNotFound();
            }
            return View(rubric);
        }

        // GET: Rubrics/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Rubrics/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name")] Rubric rubric)
        {
            if (ModelState.IsValid)
            {
                unitOfWork.Repository<Rubric>().Add(rubric);
                unitOfWork.SaveChanges();
                return RedirectToAction("Index", "Moderator");
            }

            return RedirectToAction("Index", "Moderator");
        }

        // GET: Rubrics/Edit/5
        public ActionResult Edit(int id = 0)
        {
            Rubric rubric = unitOfWork.Repository<Rubric>().Get(c => c.Id == id);
            if (rubric == null)
            {
                return HttpNotFound();
            }
            return View(rubric);
        }

        // POST: Rubrics/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name")] Rubric rubric)
        {
            if (ModelState.IsValid)
            {
                unitOfWork.Repository<Rubric>().Attach(rubric);
                unitOfWork.SaveChanges();
                return RedirectToAction("Index", "Moderator");
            }
            return RedirectToAction("Index", "Moderator");
        }

        // GET: Rubrics/Delete/5
        public ActionResult Delete(int id = 0)
        {
            Rubric rubric = unitOfWork.Repository<Rubric>().Get(c => c.Id == id);
            if (rubric == null)
            {
                return HttpNotFound();
            }
            return View(rubric);
        }

        // POST: Rubrics/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Rubric rubric = unitOfWork.Repository<Rubric>().Get(c => c.Id == id);
            unitOfWork.Repository<Rubric>().Delete(rubric);
            unitOfWork.SaveChanges();
            return RedirectToAction("Index", "Moderator");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

