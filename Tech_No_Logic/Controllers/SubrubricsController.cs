﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Barter.DAL;
using Tech_No_Logic.Models;

namespace Tech_No_Logic.Controllers
{
    [Authorize(Roles = "Moderator")]
    public class SubrubricsController : Controller
    {
        private UnitOfWork unitOfWork = null;

        public SubrubricsController()
        {
            unitOfWork = new UnitOfWork();
        }

        public SubrubricsController(UnitOfWork unitOfWork)
        {
            this.unitOfWork = unitOfWork;
        }

        // GET: Subrubrics
        public ViewResult Index()
        {
            return View(unitOfWork.Repository<Subrubric>().GetAll() as IEnumerable<Subrubric>);
        }

        // GET: Subrubrics/Details/5
        public ActionResult Details(int id = 0)
        {
            Subrubric subrubric = unitOfWork.Repository<Subrubric>().Get(c => c.Id == id);
            if (subrubric == null)
            {
                return HttpNotFound();
            }
            return View(subrubric);
        }

        // GET: Subrubrics/Create
        public ActionResult Create()
        {
            ViewBag.RubricId = new SelectList(unitOfWork.Repository<Rubric>().GetAll() as IEnumerable<Rubric>, "Id", "Name");
            return View();
        }

        // POST: Subrubrics/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Subrubric subrubric)
        {
            if (ModelState.IsValid)
            {
                unitOfWork.Repository<Subrubric>().Add(subrubric);
                unitOfWork.SaveChanges();
                return RedirectToAction("Index", "Moderator");
            }
            return RedirectToAction("Index", "Moderator");
        }

        // GET: Subrubrics/Edit/5
        public ActionResult Edit(int id = 0)
        {
            Subrubric subrubric = unitOfWork.Repository<Subrubric>().Get(c => c.Id == id);
            if (subrubric == null)
            {
                return HttpNotFound();
            }

            ViewBag.RubricId = new SelectList(unitOfWork.Repository<Rubric>().GetAll() as IEnumerable<Rubric>, "Id", "Name", subrubric.RubricId);

            return View(subrubric);

        }

        // POST: Subrubrics/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Subrubric subrubric)
        {
            if (subrubric.Name != string.Empty)
            {
                unitOfWork.Repository<Subrubric>().Attach(subrubric);
                unitOfWork.SaveChanges();

                return RedirectToAction("Index", "Moderator");
            }
            return RedirectToAction("Index", "Moderator");

        }

        // GET: Subrubrics/Delete/5
        public ActionResult Delete(int id = 0)
        {
            Subrubric subrubric = unitOfWork.Repository<Subrubric>().Get(c => c.Id == id);
            if (subrubric == null)
            {
                return HttpNotFound();
            }

            return View(subrubric);
        }

        // POST: Subrubrics/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Subrubric subrubric = unitOfWork.Repository<Subrubric>().Get(c => c.Id == id);
            unitOfWork.Repository<Subrubric>().Delete(subrubric);
            unitOfWork.SaveChanges();
            return RedirectToAction("Index", "Moderator");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                unitOfWork.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
