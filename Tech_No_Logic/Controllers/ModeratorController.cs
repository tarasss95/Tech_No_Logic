﻿using Barter.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Tech_No_Logic.Models;

namespace Tech_No_Logic.Controllers
{
    [Authorize(Roles = "Moderator")]
    public class ModeratorController : Controller
    {
        private UnitOfWork unitOfWork;

        public ModeratorController()
        {
            unitOfWork = new UnitOfWork();
        }
        // GET: Moderator
        public ActionResult Index()
        {
            return View();
        }
        
        public ActionResult Rubrics()
        {
            return PartialView("~/Views/Rubrics/Index.cshtml", unitOfWork.Repository<Rubric>().GetAll() as IEnumerable<Rubric>);
        }
        
        public ActionResult Subrubrics()
        {
            return PartialView("~/Views/Subrubrics/Index.cshtml", unitOfWork.Repository<Subrubric>().GetAll() as IEnumerable<Subrubric>);
        }
    }
}