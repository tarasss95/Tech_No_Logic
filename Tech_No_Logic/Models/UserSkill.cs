﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Tech_No_Logic.Models
{
    /// <summary>
    /// Представляє "скіл" конкретного юзера
    /// </summary>
    public class UserSkill
    {
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Посилання на підрубрику 
        /// </summary>
        public int SubrubricId { get; set; }

        [ForeignKey("SubrubricId")]
        public virtual Subrubric Subrubric { get; set; }


        /// <summary>
        /// Посилання на юзера
        /// </summary>
        [Required]
        public string UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser ApplicationUser { get; set; }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        public virtual ICollection<Feedback> Feedbacks { get; set; }
        public virtual ICollection<Request> Requests { get; set; }
        public UserSkill()
        {
            Feedbacks = new List<Feedback>();
            Requests = new List<Request>();
        }
    }
}