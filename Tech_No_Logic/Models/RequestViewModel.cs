﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Tech_No_Logic.Models
{
    public class RequestViewModel
    {
        // skill you are sugguesting
        [Display(Name = "Your skill")]
        public int SkillIdFrom { get; set; }

        [Display(Name = "Message")]
        public string Message { get; set; }
    }
}