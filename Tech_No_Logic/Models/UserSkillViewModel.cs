﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Tech_No_Logic.Models
{
    public class UserSkillViewModel
    {
        [Required]
        [Display(Name = "Subrubrics")]
        public int SubrubricId { get; set; }

        [Required]
        [Display(Name = "Skill")]
        public string Name { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }
    }
}