﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Tech_No_Logic.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {

        [Required]
        public string FName { get; set; }

        [Required]
        public string LName { get; set; }

        public int Age { get; set; }

        [Required]
        public string Hometown { get; set; }

        public byte[] Photo { get; set; }

        public virtual ICollection<UserSkill> Skills { get; set; }
        public virtual ICollection<UserSkillWanted> SkillsWanted { get; set; }

        // наразі вхідні та вихідні разом, але там можна фільтрувати легко за навігаційними проп.
        public virtual ICollection<Request> Requests { get; set; }
        public ApplicationUser()
        {
            Skills = new List<UserSkill>();
            SkillsWanted = new List<UserSkillWanted>();
            Requests = new List<Request>();
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    //public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    //{
    //    public ApplicationDbContext()
    //        : base("DefaultConnection", throwIfV1Schema: false)
    //    {
    //    }

    //    protected override void OnModelCreating(DbModelBuilder modelBuilder)
    //    {
    //        //modelBuilder.Entity<Request>()
    //        //            .HasRequired(t => t.SkillFrom)
    //        //            .WithMany(t => t.Requests)
    //        //            .WillCascadeOnDelete(true);

    //        //modelBuilder.Entity<Request>()
    //        //           .HasRequired(t => t.SkillTo)
    //        //           .WithMany(t => t.Requests)
    //        //           .WillCascadeOnDelete(true);

    //        ////base.OnModelCreating(modelBuilder);

           
    //        modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
    //        modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
    //        base.OnModelCreating(modelBuilder);
    //    }

    //    public static ApplicationDbContext Create()
    //    {
    //        return new ApplicationDbContext();
    //    }
    //}
}