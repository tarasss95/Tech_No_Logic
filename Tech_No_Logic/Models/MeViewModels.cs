﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Tech_No_Logic.Models
{
    // Models returned by MeController actions.
    public class GetViewModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public string Hometown { get; set; }
        public byte[] Photo { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public virtual ICollection<UserSkill> Skills { get; set; }
        public virtual ICollection<UserSkillWanted> SkillsWanted { get; set; }

    }
}