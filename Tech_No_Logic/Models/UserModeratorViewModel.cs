﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Tech_No_Logic.Models
{
    public class UserModeratorViewModel
    {

        public string Id { get; set; }

        [Display(Name = "Firstname")]
        public string FName { get; set; }

        [Display(Name = "Lastname")]
        public string LName { get; set; }

        public bool isModerator { get; set; }
    }
}