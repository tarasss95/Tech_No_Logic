﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Tech_No_Logic.Models
{
    public class Rubric
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(40, ErrorMessage = "Максимальна довжина {2}", MinimumLength = 2)]
        public string Name { get; set; }

        public Rubric()
        {
            Subrubrics = new List<Subrubric>();
        }
        // navigation property
        public virtual ICollection<Subrubric> Subrubrics { get; set; }        
    }
}