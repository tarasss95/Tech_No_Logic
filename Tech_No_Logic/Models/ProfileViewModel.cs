﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Tech_No_Logic.Models
{
    public class ProfileViewModel
    {
        public string Id { get; set; }
        public string PhoneNumber { get; set; }
        public string HomeTown { get; set; }
        public byte[] Photo { get; set; }
        public string UserEmail { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
    }
}