﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using Tech_No_Logic.Models;

namespace Barter.DAL
{
    public class BarterContext : IdentityDbContext<ApplicationUser>
    {
        public BarterContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //base.OnModelCreating(modelBuilder);

            //modelBuilder.Entity<Request>()
            //            .HasRequired(t => t.SkillFrom)
            //            .WithMany(t => t.OutputRequests)
            //            .WillCascadeOnDelete(true);

            //modelBuilder.Entity<Request>()
            //           .HasRequired(t => t.SkillTo)
            //           .WithMany(t => t.Requests)
            //           .WillCascadeOnDelete(true);


            base.OnModelCreating(modelBuilder);
           // modelBuilder.Entity<Subrubric>().Property(e => e.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

        }

        public static BarterContext Create()
        {
            return new BarterContext();
        }

    }
    public class ItemInitializer : DropCreateDatabaseAlways<BarterContext>
    {
        protected override void Seed(BarterContext context)
        {
            //Initialize Roles
            UserManager<ApplicationUser> userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            string adminRoleName = "Admin";
            string moderatorRoleName = "Moderator";
            string userRoleName = "User";
            if (!roleManager.RoleExists(moderatorRoleName))
            {
                var role = new IdentityRole(moderatorRoleName);
                roleManager.Create(role);
            }
            if (!roleManager.RoleExists(adminRoleName))
            {
                var role = new IdentityRole(adminRoleName);
                roleManager.Create(role);
            }
            if (!roleManager.RoleExists(userRoleName))
            {
                var role = new IdentityRole(userRoleName);
                roleManager.Create(role);
            }
            CreateTemplateUser("Admin@gmail.com", "Admin", "Admin", "Admin@gmail.com", "Qwerty123", userManager,
                adminRoleName);
            CreateTemplateUser("Moderator@gmail.com", "ModeratorFirstName", "ModeratorLastName", "Moderator@gmail.com", "Qwerty123", userManager,
                moderatorRoleName);
            CreateTemplateUser("User@gmail.com", "UserFirstName", "UserLastName", "User@gmail.com", "Qwerty123", userManager,
                userRoleName);

            //Initialize rubrics and subrubrics

            var SportAndHealth = new Rubric { Name = "Sport and health"};
            var ScienceAndAducation = new Rubric { Name = "Science and Aducation"};
            var Entertainment = new Rubric { Name = "Entertainment"};
            var ArtDesign = new Rubric { Name = "Art, design"};
            var NatureAndTravelling = new Rubric { Name = "Nature and travelling"};
            var FamilyAndHome = new Rubric { Name = "Family and home"};
            var LiteratureAndPoetry = new Rubric { Name = "Literature and poetry"};
            var Society = new Rubric { Name = "Society"};
            var Another = new Rubric { Name = "Another"};
            context.Set<Rubric>().Add(SportAndHealth);
            context.Set<Rubric>().Add(ScienceAndAducation);
            context.Set<Rubric>().Add(Entertainment);
            context.Set<Rubric>().Add(ArtDesign);
            context.Set<Rubric>().Add(NatureAndTravelling);
            context.Set<Rubric>().Add(FamilyAndHome);
            context.Set<Rubric>().Add(LiteratureAndPoetry);
            context.Set<Rubric>().Add(Society);
            context.Set<Rubric>().Add(Another);

            var Football = new Subrubric { Name = "Football", Rubric=SportAndHealth };
            var Fitness = new Subrubric { Name = "Fitness", Rubric = SportAndHealth };
            var SportNutrition = new Subrubric { Name = "Sport nutrition", Rubric = SportAndHealth };
            var SportAnother = new Subrubric { Name = "Sport(Another)", Rubric = SportAndHealth };
            var Science = new Subrubric { Name = "Science", Rubric = ScienceAndAducation };
            var ForeignLanguages = new Subrubric { Name = "Foreign languages", Rubric = ScienceAndAducation };
            var Technologics = new Subrubric { Name = "Technologics", Rubric = ScienceAndAducation };
            var Films = new Subrubric { Name = "Films", Rubric = Entertainment };
            var Music = new Subrubric { Name = "Music", Rubric = Entertainment };
            var Games = new Subrubric { Name = "Games", Rubric = Entertainment };
            var Architecture = new Subrubric { Name = "Architecture", Rubric = ArtDesign };
            var Pictures = new Subrubric { Name = "Pictures", Rubric = ArtDesign };
            var Design = new Subrubric { Name = "Design", Rubric = ArtDesign };
            var Nature = new Subrubric { Name = "Nature", Rubric = NatureAndTravelling };
            var Travelling = new Subrubric { Name = "Travelling", Rubric = NatureAndTravelling };
            var Kids = new Subrubric { Name = "Kids", Rubric = FamilyAndHome };
            var Mariage = new Subrubric { Name = "Mariage", Rubric = FamilyAndHome };
            var ArrangementAndRepair = new Subrubric { Name = "Arrangement and repair", Rubric = FamilyAndHome };
            var Books = new Subrubric { Name = "Books", Rubric = LiteratureAndPoetry };
            var Poem = new Subrubric { Name = "Poem", Rubric = LiteratureAndPoetry };
            var Proffesions = new Subrubric { Name = "Proffesions", Rubric = Society };
            var Politics = new Subrubric { Name = "Politics", Rubric = Society };
            var Religion = new Subrubric { Name = "Religion", Rubric = Society };
            var Fashion = new Subrubric { Name = "Fashion", Rubric = Another };
            var Cooking = new Subrubric { Name = "Cooking", Rubric = Another };
            context.Set<Subrubric>().Add(Football);
            context.Set<Subrubric>().Add(Fitness);
            context.Set<Subrubric>().Add(SportNutrition);
            context.Set<Subrubric>().Add(SportAnother);
            context.Set<Subrubric>().Add(Science);
            context.Set<Subrubric>().Add(ForeignLanguages);
            context.Set<Subrubric>().Add(Technologics);
            context.Set<Subrubric>().Add(Films);
            context.Set<Subrubric>().Add(Music);
            context.Set<Subrubric>().Add(Games);
            context.Set<Subrubric>().Add(Architecture);
            context.Set<Subrubric>().Add(Pictures);
            context.Set<Subrubric>().Add(Design);
            context.Set<Subrubric>().Add(Nature);
            context.Set<Subrubric>().Add(Travelling);
            context.Set<Subrubric>().Add(Kids);
            context.Set<Subrubric>().Add(Mariage);
            context.Set<Subrubric>().Add(ArrangementAndRepair);
            context.Set<Subrubric>().Add(Books);
            context.Set<Subrubric>().Add(Poem);
            context.Set<Subrubric>().Add(Proffesions);
            context.Set<Subrubric>().Add(Politics);
            context.Set<Subrubric>().Add(Religion);
            context.Set<Subrubric>().Add(Fashion);
            context.Set<Subrubric>().Add(Cooking);
            
            //Initialize users and their skills
            string password = "Qwerty123";
            ApplicationUser user1 = new ApplicationUser
            {
                UserName = "OlenaGrach@gmail.com",FName = "Olena",LName = "Grach",Hometown = "Kyyv",EmailConfirmed = true,Email = "OlenaGrach@gmail.com",Age = 23
            };
            UserSkill skill1 = new UserSkill { UserId=user1.Id, Subrubric = SportNutrition, Name = "Chocolate diet", Description = "I can teach you this diet" };
            UserSkill skill2 = new UserSkill { UserId = user1.Id, Subrubric = Music, Name = "Playing piano", Description = "I am playing piano for 10 years" };
          

            ApplicationUser user2 = new ApplicationUser
            {
                UserName = "IvanBoyko@gmail.com",FName = "Ivan",LName = "Boyko", Hometown = "Ivano-Frankivsk",EmailConfirmed = true,Email = "IvanBoyko@gmail.com",Age = 35
            };
            UserSkill skill3 = new UserSkill { UserId = user2.Id, Subrubric = ForeignLanguages, Name = "English language", Description = "my level of english - Anvanced" };
            UserSkill skill4 = new UserSkill { UserId = user2.Id, Subrubric = Cooking, Name = "Cooking pizza", Description = "Very taste pizza" };
       

            ApplicationUser user3 = new ApplicationUser
            {
                 UserName = "NataliaFityo@gmail.com", FName = "Natalia", LName = "Fityo", Hometown = "Ternopil", EmailConfirmed = true, Email = "NataliaFityo@gmail.com", Age = 18
            };
            UserSkill skill5 = new UserSkill { UserId = user3.Id, Subrubric = Science, Name = "Phisics", Description = "I can teach you to test" };
            UserSkill skill6 = new UserSkill { UserId = user3.Id, Subrubric = Science, Name = "Phisics", Description = "I can teach you to test" };
        

            ApplicationUser user4 = new ApplicationUser
            {
                 UserName = "AndriyVoloshyn@gmail.com", FName = "Andriy", LName = "Voloshyn", Hometown = "Poltava", EmailConfirmed = true, Email = "AndriyVoloshyn@gmail.com", Age = 27
            };
            UserSkill skill7 = new UserSkill { UserId = user4.Id, Subrubric = Travelling, Name = "Expanding of tent", Description = "When I was 10 I made it at first time " };
     

            ApplicationUser user5 = new ApplicationUser
            {
                UserName = "IgorKonovalov@gmail.com", FName = "Igor", LName = "Konovalov", Hometown = "Dnipro", EmailConfirmed = true, Email = "IgorKonovalov@gmail.com", Age = 30
            };
            UserSkill skill8 = new UserSkill { UserId = user5.Id, Subrubric = Architecture, Name = "Architecting", Description = "I can archtect everything" };
        

            ApplicationUser user6 = new ApplicationUser
            {
                UserName = "AnnaGerman@gmail.com", FName = "Anna", LName = "German", Hometown = "Kharkiv", EmailConfirmed = true, Email = "AnnaGerman@gmail.com", Age = 37
            };
            UserSkill skill9 = new UserSkill { UserId = user6.Id, Subrubric = Fashion, Name = "Sewing suit", Description = "Sewing of anyone suit you want" };
            UserSkill skill10 = new UserSkill { UserId = user6.Id, Subrubric = Music, Name = "Playing piano", Description = "I am playing piano for 20 years" };


            ApplicationUser user7 = new ApplicationUser
            {
                UserName = "MykhayloZavadovskiy@gmail.com", FName = "Mykhaylo", LName = "Zavadovskiy", Hometown = "Chernivtsi", EmailConfirmed = true, Email = "MykhayloZavadovskiy@gmail.com", Age = 20
            };
            UserSkill skill11 = new UserSkill { UserId = user7.Id, Subrubric = Technologics, Name = "Repair of electrotic devices", Description = "I can teach you it" };
            

            ApplicationUser user8 = new ApplicationUser
            {
                UserName = "KristinaTarasiuk@gmail.com", FName = "Kristina", LName = "Tarasiuk", Hometown = "Lviv", EmailConfirmed = true, Email = "KristinaTarasiuk@gmail.com", Age = 19
            };
            UserSkill skill12 = new UserSkill { UserId = user8.Id, Subrubric = Proffesions, Name = "Economist", Description = "I can make you like wolf from Wall street" };
            

            ApplicationUser user9 = new ApplicationUser
            {
                 UserName = "JohnCena@gmail.com", FName = "John", LName = "Cena", Hometown = "Kyyv", EmailConfirmed = true, Email = "JohnCena@gmail.com", Age = 38
            };
            UserSkill skill13 = new UserSkill { UserId = user9.Id, Subrubric = SportAnother, Name = "Wresling", Description = "You become a star of WWE" };
            

            ApplicationUser user10 = new ApplicationUser
            {
                UserName = "SpiderMan@gmail.com", FName = "Spider", LName = "Man", Hometown = "Kolomyya", EmailConfirmed = true, Email = "SpiderMan@gmail.com", Age = 100
            };
            UserSkill skill14 = new UserSkill { UserId = user10.Id, Subrubric = Religion, Name = "God", Description = "I've make you to belive in God" };
            

            userManager.Create(user1, password);
            userManager.Create(user2, password);
            userManager.Create(user3, password);
            userManager.Create(user4, password);
            userManager.Create(user5, password);
            userManager.Create(user6, password);
            userManager.Create(user7, password);
            userManager.Create(user8, password);
            userManager.Create(user9, password);
            userManager.Create(user10, password);
            context.Set<UserSkill>().Add(skill1);
            context.Set<UserSkill>().Add(skill2);
            context.Set<UserSkill>().Add(skill3);
            context.Set<UserSkill>().Add(skill4);
            context.Set<UserSkill>().Add(skill5);
            context.Set<UserSkill>().Add(skill6);
            context.Set<UserSkill>().Add(skill7);
            context.Set<UserSkill>().Add(skill8);
            context.Set<UserSkill>().Add(skill9);
            context.Set<UserSkill>().Add(skill10);
            context.Set<UserSkill>().Add(skill11);
            context.Set<UserSkill>().Add(skill12);
            context.Set<UserSkill>().Add(skill13);
            context.Set<UserSkill>().Add(skill14);
            context.SaveChanges();
            base.Seed(context);
        }

        private ApplicationUser CreateTemplateUser(string nick,
                                    string first, string last, string email, string pass,
                                    UserManager<ApplicationUser> userManager, string role)
        {
            ApplicationUser user = userManager.FindByName(nick);
            if (user == null)
            {
                userManager.Create(
                    new ApplicationUser { UserName = nick, FName = first, LName = last, Email = email, Hometown = "Lviv", EmailConfirmed=true }, pass);
                user = userManager.FindByName(nick);
            }
            if (!userManager.IsInRole(user.Id, role))
            {
                userManager.AddToRole(user.Id, role);
            }
            return user;
        }
    }

}
