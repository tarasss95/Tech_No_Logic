﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;

namespace Barter.DAL
{
    class GenericRepository<T> : IRepository<T> where T : class
    {
        private BarterContext entities = null;
        IDbSet<T> objectSet;

        public GenericRepository(BarterContext _entities)
        {
            this.entities = _entities;
            objectSet = entities.Set<T>();
        }

        public IEnumerable<T> GetAll(Func<T, bool> predicate = null)
        {
            if (predicate != null)
            {
                return objectSet.Where(predicate);
            }

            return objectSet.ToList();
        }

        public T Get(Func<T, bool> predicate)
        {
            return objectSet.First(predicate);
        }

        public void Add(T entity)
        {
            objectSet.Add(entity);
        }

        public void Attach(T entity)
        {
            objectSet.Attach(entity);
            entities.Entry(entity).State = EntityState.Modified;
        }

        public void Delete(T entity)
        {
            objectSet.Remove(entity);
        }
    }
}