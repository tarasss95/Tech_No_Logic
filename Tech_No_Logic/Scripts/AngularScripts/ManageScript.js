﻿var app = angular.module("Barter", []);

app.controller("ManageController", function ($scope) {

    $scope.changeMainInfo = "/Manage/MainInfo";

    $scope.changePhoto = "/Manage/ChangePhoto";

    $scope.security = "/Manage/Security";

    $scope.url = $scope.changeMainInfo;

    $scope.showChangeMainInfo = function () {
        $scope.url = $scope.changeMainInfo;
    }

    $scope.showChangePhoto = function () {
        $scope.url = $scope.changePhoto;
    }
    $scope.showSecurity = function () {
        $scope.url = $scope.security;
    }
});
